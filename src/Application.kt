package me.goto1134

import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.html.Placeholder
import io.ktor.html.Template
import io.ktor.html.insert
import io.ktor.html.respondHtmlTemplate
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.request.path
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.webjars.Webjars
import kotlinx.css.CSSBuilder
import kotlinx.css.Color
import kotlinx.css.FontStyle
import kotlinx.css.FontWeight
import kotlinx.css.a
import kotlinx.css.color
import kotlinx.css.fontFamily
import kotlinx.css.fontSize
import kotlinx.css.fontStyle
import kotlinx.css.fontWeight
import kotlinx.css.marginRight
import kotlinx.css.px
import kotlinx.html.BODY
import kotlinx.html.CommonAttributeGroupFacade
import kotlinx.html.FlowContent
import kotlinx.html.FlowOrMetaDataContent
import kotlinx.html.HTML
import kotlinx.html.TITLE
import kotlinx.html.a
import kotlinx.html.b
import kotlinx.html.body
import kotlinx.html.div
import kotlinx.html.h2
import kotlinx.html.h6
import kotlinx.html.head
import kotlinx.html.i
import kotlinx.html.section
import kotlinx.html.style
import kotlinx.html.styleLink
import kotlinx.html.title
import org.slf4j.event.Level

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused", "LongMethod")
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    install(Webjars)

    routing {
        route("/api") {
            get("/alive") {
                call.respond(HttpStatusCode.OK)
            }

            get("/ready") {
                call.respond(HttpStatusCode.OK)
            }
        }

        get("/") {
            call.respondHtmlTemplate(MainTemplate()) {
                titleText {
                    +"goto1134"
                }
                body {
                    div(classes = "container") {
                        section(classes = "header") {
                            h2(classes = "title") {
                                +"You landed "
                                b { +"goto1134" }
                                +"'s place"
                            }
                        }
                        div(classes = "row") {
                            div(classes = "six columns") {
                                h6 { +"Contacts:" }
                                contact("fab fa-telegram", "https://t.me/goto1134")
                                contact("fab fa-vk", "https://vk.com/goto1134")
                                contact("fas fa-envelope", "mailto:1134togo@gmail.com", "1134togo@gmail.com")
                            }
                            div(classes = "six columns") {
                                h6 { +"Repositories:" }
                                contact("fab fa-github", "https://github.com/goto1134")
                                contact("fab fa-gitlab", "https://gitlab.com/goto1134")
                            }
                        }
                    }
                }
            }
        }

        @Suppress("MagicNumber")
        get("/style.css") {
            call.respondCss {
                /*
                  Font settings taken from https://github.com/JetBrains/JetBrainsMono
                 */
                fontFace {
                    fontFamily = "'Jetbrains Mono'"
                    put(
                        "src", @Suppress("MaxLineLength") """
                        url('https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/web/eot/JetBrainsMono-Regular.eot') format ('embedded-opentype'),
                        url('https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/web/woff2/JetBrainsMono-Regular.woff2') format ('woff2'),
                        url('https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/web/woff/JetBrainsMono-Regular.woff') format ('woff'),
                        url('https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/ttf/JetBrainsMono-Regular.ttf') format ('truetype')
                        """.trimIndent().trimMargin()
                    )
                    fontWeight = FontWeight.normal
                    fontStyle = FontStyle.normal
                }
                universal {
                    fontFamily = "'Jetbrains Mono'"
                    put("-webkit-font-feature-settings", "\"liga\" on, \"calt\" on")
                    put("-webkit-font-smoothing", "antialiased")
                    put("text-rendering", "optimizeLegibility")
                }

                rule(".gotocontact i") {
                    fontSize = 20.px
                    marginRight = 10.px
                }

                a {
                    color = Color.black
                }
            }
        }

        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { cause ->
                call.respond(HttpStatusCode.Forbidden)
            }
        }
    }
}

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}

class MainTemplate : Template<HTML> {
    val titleText = Placeholder<TITLE>()
    val body = Placeholder<BODY>()
    override fun HTML.apply() {
        head {
            title {
                insert(titleText)
            }
            styleLink(url = "/webjars/font-awesome/css/all.min.css")
            styleLink(url = "/webjars/skeleton-css/css/skeleton.css")
            styleLink(url = "/style.css")
        }
        body {
            insert(body)
        }
    }
}

fun FlowContent.contact(
    icon: String,
    link: String,
    text: String = "@goto1134"
) = div(classes = "gotocontact") {
    a(target = "_blank", href = link) {
        i(classes = icon)
        b { +text }
    }
}
