FROM adoptopenjdk:14-jre-hotspot-bionic
MAINTAINER Andrey Efanov <1134togo@gmail.com>

RUN adduser ktor
USER ktor
COPY --chown=ktor ./build/libs/goto-home-0.0.1-all.jar ./goto-home.jar

EXPOSE 8080

CMD ["java", "-server", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "./goto-home.jar"]
