
fatJar:
	./gradlew build

docker: fatJar
	docker build --platform=linux/amd64,linux/arm/v7 -t registry.gitlab.com/goto1134-homelab/goto1134-home:local-build . --push

deploy: fatJar docker
	helm upgrade --install --recreate-pods -n playground goto-home goto-home
